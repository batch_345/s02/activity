package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String title;
    @Column
    private String content;

    public Post(){}
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getUsername() { return title; }

    public void setUsername(String username) { this.title = username; }

    public String getPassword() { return  content; }

    public void setPassword(String password) { this.content = password; }
}